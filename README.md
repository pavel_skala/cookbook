# Ackee Cookbook Android Version

Welcome! Wanna join [Ackee][1] ? Or you just don't know what to do on lazy sunday afternoon ?

Here is a Task for you! Create Android version of delicious ackee recipes. We have everything you need to get started
- API (and running server)
- Designs
- Simple Android skeleton with all icons and assets (including Roboto font)

it should take only a few hours to complete

## API & Server
Documentation for the api can be found on [apiary][2]
live server can be visited on address [https://cookbook.ack.ee][3]

## Designs
Application should contain those 3 screens. The sketch can be found [here][4]

<img src="https://raw.githubusercontent.com/AckeeCZ/cookbook-android-task/master/screens/01_list.png" width="200">&nbsp;&nbsp;&nbsp;
<img src="https://raw.githubusercontent.com/AckeeCZ/cookbook-android-task/master/screens/03_add.png" width="200">&nbsp;
<img src="https://raw.githubusercontent.com/AckeeCZ/cookbook-android-task/master/screens/02_detail.png" width="200">&nbsp;&nbsp;&nbsp;


- List of recipes
- Form for adding a new recipe (accessed via plus button)
- Recipe detail with rating (accessed via click in the list)

### Tasks for you

- Write the app :)
- Use as much standards as we do in our [Android Cookbook][5]
- Using reactive programming and MVP Architecture is a big advantage
- There are only few recipes on server, however you should take in mind that the application should deal with any number of recipes!
- Use 3rd party libraries, technologies and frameworks as you like. Have a good reason to use any of them, though.  If you will use Kotlin, we will love you.
- If you want to add anything extra, just go for it!
- Send it to us as link to dropbox / GDrive / etc..

[1]:	https://ackee.cz
[2]:	http://docs.cookbook3.apiary.io/#introduction/recipes
[3]:	https://cookbook.ack.ee
[4]:	https://raw.githubusercontent.com/AckeeCZ/cookbook-android-task/master/screens/ackee_cookbook.sketch
[5]:	https://github.com/AckeeCZ/android-cookbook