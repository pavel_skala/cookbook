package cz.ackee.cookbook

import android.app.Application
import cz.ackee.cookbook.di.DaggerNetworkComponent
import cz.ackee.cookbook.di.NetworkComponent
import cz.ackee.cookbook.util.delegates.DelegatesExt

/**
 * Created by Pavel on 24.1.2017.
 */
class CookbookApplication : Application() {

    companion object {
        /**
         * The main DI component, initialized on app start
         */
        var networkComponent: NetworkComponent by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        networkComponent = DaggerNetworkComponent.builder().build()
    }

}