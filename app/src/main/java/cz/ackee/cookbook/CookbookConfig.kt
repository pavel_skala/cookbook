package cz.ackee.cookbook

/**
 * Configuration constants of the app
 *
 * Created by Pavel on 25.1.2017.
 */
object CookbookConfig {

    /**
     * Base URL of the api to retrieve data from. Could be loaded from BuildConfig
     * when multiple environments are present.
     */
    val API_BASE_URL = "https://cookbook.ack.ee/api/v1/"

}