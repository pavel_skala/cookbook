package cz.ackee.cookbook.di

import cz.ackee.cookbook.view.AddRecipeActivity
import cz.ackee.cookbook.view.DetailFragment
import cz.ackee.cookbook.view.RecipesListFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Pavel on 25.1.2017.
 *
 * DI Component supplying instances of network related classes
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class))
interface NetworkComponent {

    fun inject(fragment: RecipesListFragment)
    fun inject(fragment: DetailFragment)
    fun inject(activity: AddRecipeActivity)

}