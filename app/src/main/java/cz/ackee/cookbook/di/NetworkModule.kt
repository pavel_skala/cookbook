package cz.ackee.cookbook.di

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import cz.ackee.cookbook.CookbookConfig
import cz.ackee.cookbook.model.CookbookClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * DI Module responsible for creation of Retrofit and the Retrofit client
 * for communication with the API
 *
 * Created by Pavel on 25.1.2017.
 */
@Module
class NetworkModule {

    /**
     * The main network client for direct communication with API, typically there would also
     * be another domain level which would transform communication model to business model.
     * For the simplicity this level was omitted.
     */
    @Provides
    @Singleton
    fun provideCookbookClient(retrofit: Retrofit) = retrofit.create(CookbookClient::class.java)!!

    /**
     * Singleton instance of Retrofit used by the client
     */
    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BODY
//        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
                .baseUrl(CookbookConfig.API_BASE_URL)
//                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

}
