package cz.ackee.cookbook.model

/**
 * Representation of recipe without detailed information (as in list of recipes)
 */
class RecipeSimple {
    var id: String? = null
    var name: String? = null
    var duration: Int? = null
    var score: Float? = null
}

/**
 * Full representation of recipe detail
 */
class Recipe {
    var id: String? = null
    var name: String? = null
    var description: String? = null
    var duration: Int? = null
    var ingredients: List<String>? = null
    var info: String? = null
    var score: Float? = null
}

/**
 * Body of request for adding a new rating
 */
class RatingRequest {
    var score: Int? = null
}

/**
 * Response of the request for adding a new rating
 */
class Rating {
    var id: String? = null
    var score: Float? = null
}
