package cz.ackee.cookbook.model

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

/**
 * Interface for communication with the remote API using the Retrofit library
 *
 * Created by Pavel on 25.1.2017.
 */
interface CookbookClient {

    /**
     * Retrieves the list of all recipes
     */
    @GET("recipes")
    fun getAllRecipes(): Single<List<RecipeSimple>>

    /**
     * Adds new recipe to the server
     */
    @POST("recipes")
    fun addRecipe(@Body recipe: Recipe): Single<Recipe>

    /**
     * Retrieves detail of single recipe
     */
    @GET("recipes/{id}")
    fun getRecipe(@Path("id") id: String): Single<Recipe>

    /**
     * Deletes a recipe
     */
    @DELETE("recipes/{id}")
    fun deleteRecipe(@Path("id") id: String): Completable

    /**
     * Adds rating to the given recipe
     */
    @POST("recipes/{id}/ratings")
    fun addRating(@Path("id") id: String, @Body rating: RatingRequest): Single<Rating>

}