package cz.ackee.cookbook.util.extensions

import io.reactivex.plugins.RxJavaPlugins

/**
 * Created by Pavel on 27.1.2017.
 */

fun String.isNumericPositive(): Boolean =
    try {
        val number = this.toInt()
        number >= 0
    } catch (e: NumberFormatException) {
        false
    }
