package cz.ackee.cookbook.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import cz.ackee.cookbook.CookbookApplication
import cz.ackee.cookbook.R
import cz.ackee.cookbook.databinding.ActivityAddRecipeBinding
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.viewmodel.AddRecipeActivityViewModel
import kotlinx.android.synthetic.main.activity_add_recipe.*
import org.jetbrains.anko.toast
import javax.inject.Inject

class AddRecipeActivity : AppCompatActivity(), AddRecipeActivityViewModel.DataListener {

    @Inject lateinit var client: CookbookClient
    private lateinit var viewModel: AddRecipeActivityViewModel
    private lateinit var binding: ActivityAddRecipeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CookbookApplication.networkComponent.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_recipe)
        viewModel = AddRecipeActivityViewModel(client, this, this)
        if (savedInstanceState != null) {
            viewModel.restoreState(savedInstanceState)
        }
        binding.viewModel = viewModel
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_add_recipe, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_recipe_add -> {
                if (viewModel.validate()) {
                    viewModel.saveRecipe()
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.saveState(outState)
    }

    override fun saveSuccessful() {
        finish()
    }

    override fun showAddError(e: Throwable) {
        toast(R.string.error_recipe_add)
    }

}
