package cz.ackee.cookbook.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import cz.ackee.cookbook.R
import cz.ackee.cookbook.viewmodel.DetailActivityViewModel

class DetailActivity : AppCompatActivity() {

    companion object {
        val EXTRA_RECIPE_ID = "cz.ackee.cookbook.extra_recipe_id"
        val EXTRA_RECIPE_NAME = "cz.ackee.cookbook.extra_recipe_name"
        val EXTRA_RECIPE_SCORE = "cz.ackee.cookbook.extra_recipe_score"
        val EXTRA_RECIPE_TIME = "cz.ackee.cookbook.extra_recipe_time"
        val TAG_FRAGMENT_DETAIL = "cz.ackee.cookbook.fragment_detail"
    }

    private val viewModel = DetailActivityViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val previousFragment = supportFragmentManager.findFragmentByTag(TAG_FRAGMENT_DETAIL)
        if (previousFragment == null) {
            val detailFragment = DetailFragment.getInstance(
                    intent.getStringExtra(EXTRA_RECIPE_ID),
                    intent.getStringExtra(EXTRA_RECIPE_NAME),
                    intent.getFloatExtra(EXTRA_RECIPE_SCORE, 0f),
                    intent.getIntExtra(EXTRA_RECIPE_TIME, 0)
            )
            supportFragmentManager.beginTransaction().add(android.R.id.content,
                    detailFragment, TAG_FRAGMENT_DETAIL).commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_recipe_add -> {
                viewModel.addRecipe()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
