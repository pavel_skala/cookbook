package cz.ackee.cookbook.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.ackee.cookbook.CookbookApplication
import cz.ackee.cookbook.R
import cz.ackee.cookbook.databinding.FragmentDetailBinding
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.viewmodel.DetailFragmentViewModel
import kotlinx.android.synthetic.main.fragment_detail.view.*
import org.jetbrains.anko.toast
import javax.inject.Inject

/**
 * Created by Pavel on 25.1.2017.
 */
class DetailFragment : Fragment(), DetailFragmentViewModel.DataListener {

    @Inject lateinit var client: CookbookClient
    private var itemId: String? = null
    private var defaultName: String? = null
    private var defaultScore: Float? = null
    private var defaultTime: Int? = null

    private lateinit var binding: FragmentDetailBinding
    private var viewModel: DetailFragmentViewModel? = null

    companion object {
        /**
         * Gets instance of DetailFragment initialized with the given data
         */
        fun getInstance(itemId: String?, name: String?, score: Float?, time: Int?): DetailFragment {
            val fragment = DetailFragment()
            fragment.itemId = itemId
            fragment.defaultName = name
            fragment.defaultScore = score
            fragment.defaultTime = time
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true // to preserve the viewModel on configuration changes
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        CookbookApplication.networkComponent.inject(this)
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_detail,
                container,
                false)
        if (viewModel == null) {
            viewModel = DetailFragmentViewModel(client)
            setDefaultsToViewModel()
        }
        binding.viewModel = viewModel
        return binding.layoutDetail
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(view.toolbar_detail)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onStart() {
        viewModel!!.attach(this)
        viewModel!!.loadData()
        super.onStart()
    }

    override fun onStop() {
        viewModel!!.detach()
        super.onStop()
    }

    override fun onDestroy() {
        viewModel!!.destroy()
        super.onDestroy()
    }

    override fun showDetailError(e: Throwable) {
        activity.toast(R.string.error_recipe)
    }

    override fun showRatingError(e: Throwable) {
        activity.toast(R.string.error_recipe_rating)
    }

    private fun setDefaultsToViewModel() {
        viewModel!!.setDefaults(itemId, defaultName, defaultScore, defaultTime)
    }

}