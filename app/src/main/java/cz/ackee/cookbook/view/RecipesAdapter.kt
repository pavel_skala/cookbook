package cz.ackee.cookbook.view

import android.databinding.DataBindingUtil
import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.ViewGroup
import cz.ackee.cookbook.R
import cz.ackee.cookbook.databinding.ItemRecipeBinding
import cz.ackee.cookbook.model.RecipeSimple
import cz.ackee.cookbook.viewmodel.RecipeItemViewModel

/**
 * Recycler Adapter for the list of all recipes
 *
 * Created by Pavel on 25.1.2017.
 */
class RecipesAdapter(private val actionsListener: ActionsListener) : RecyclerView.Adapter<RecipesAdapter.RecipeViewHolder>() {

    private val data: MutableList<RecipeSimple> = mutableListOf()

    val touchHelperCallback = object : ItemTouchHelper.SimpleCallback(0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            removeItem(viewHolder.adapterPosition)
        }

        override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
            viewHolder.itemView.alpha = (1 - Math.abs(dX / viewHolder.itemView.width))
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val binding: ItemRecipeBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_recipe,
                parent,
                false)
        val viewModel = RecipeItemViewModel(parent.context)
        binding.viewModel = viewModel
        return RecipeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.bind(data[position])
    }

    /**
     * Replaces current data of the adapter
     */
    fun setData(data: List<RecipeSimple>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun restoreItem(item: RecipeSimple, position: Int) {
        data.add(position, item)
        notifyItemInserted(position)
    }

    private fun removeItem(position: Int) {
        val item = data[position]
        data.removeAt(position)
        notifyItemRemoved(position)
        actionsListener.itemRemoved(item, position)
    }

    class RecipeViewHolder(private val binding: ItemRecipeBinding)
        : RecyclerView.ViewHolder(binding.layoutItemRecipe) {

        fun bind(recipe: RecipeSimple) {
            binding.viewModel.setData(recipe)
        }

    }

    interface ActionsListener {
        /**
         * Called when item is removed from the adapter using swipe gesture
         */
        fun itemRemoved(item: RecipeSimple, position: Int)
    }

}