package cz.ackee.cookbook.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.ackee.cookbook.CookbookApplication
import cz.ackee.cookbook.R
import cz.ackee.cookbook.databinding.FragmentRecipesListBinding
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.model.RecipeSimple
import cz.ackee.cookbook.viewmodel.RecipesListFragmentViewModel
import org.jetbrains.anko.toast
import javax.inject.Inject

/**
 * Created by Pavel on 25.1.2017.
 */
class RecipesListFragment : Fragment(), RecipesListFragmentViewModel.DataListener, RecipesAdapter.ActionsListener {

    @Inject lateinit var client: CookbookClient
    private lateinit var binding: FragmentRecipesListBinding
    private var viewModel: RecipesListFragmentViewModel? = null

    private val recipesAdapter = RecipesAdapter(this)
    private val itemTouchHelper = ItemTouchHelper(recipesAdapter.touchHelperCallback)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        CookbookApplication.networkComponent.inject(this)
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_recipes_list, container, false)
        if (viewModel == null) {
            viewModel = RecipesListFragmentViewModel(client)
        }
        binding.viewModel = viewModel
        binding.recyclerRecipes.layoutManager = LinearLayoutManager(
                activity, LinearLayoutManager.VERTICAL, false)
        binding.recyclerRecipes.adapter = recipesAdapter
        itemTouchHelper.attachToRecyclerView(binding.recyclerRecipes)
        binding.refreshRecipes.setOnRefreshListener { viewModel!!.loadData(true) }
        return binding.layoutRecipes
    }

    override fun onStart() {
        viewModel!!.attach(this)
        viewModel!!.loadData(false)
        super.onStart()
    }

    override fun onStop() {
        viewModel!!.detach()
        super.onStop()
    }

    override fun onDestroy() {
        viewModel!!.destroy()
        super.onDestroy()
    }

    override fun dataLoaded(data: List<RecipeSimple>) {
        recipesAdapter.setData(data)
    }

    override fun showLoading(loading: Boolean) {
        binding.refreshRecipes.isRefreshing = loading
    }

    override fun showLoadingError(e: Throwable) {
        activity.toast(R.string.error_recipes)
    }

    override fun itemRemoved(item: RecipeSimple, position: Int) {
        val itemName = item.name ?: ""
        Snackbar.make(
                binding.recyclerRecipes,
                activity.getString(R.string.snack_recipe_item_delete,
                    if (itemName.length > 20) "${itemName.substring(20)}..." else itemName),
                Snackbar.LENGTH_LONG)
                .setAction(R.string.snack_recipe_item_delete_cancel) {
                    recipesAdapter.restoreItem(item, position)
                }
                .setCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar, event: Int) {
                        if (event != Snackbar.Callback.DISMISS_EVENT_ACTION) {
                            viewModel!!.deleteRecipe(item, position)
                        }
                    }
                })
                .show()
    }

    override fun showDeleteError(e: Throwable, item: RecipeSimple, originalPosition: Int) {
        activity.toast(R.string.error_recipe_remove)
        recipesAdapter.restoreItem(item, originalPosition)
    }

}