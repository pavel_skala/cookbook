package cz.ackee.cookbook.viewmodel

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import cz.ackee.cookbook.BR
import cz.ackee.cookbook.R
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.model.Recipe
import cz.ackee.cookbook.util.extensions.isNumericPositive
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.tatarka.bindingcollectionadapter.ItemView
import me.tatarka.bindingcollectionadapter.LayoutManagers
import java.util.*

/**
 * Created by Pavel on 25.1.2017.
 */
class AddRecipeActivityViewModel(
        private val client: CookbookClient,
        private val dataListener: DataListener,
        private val context: Context) : BaseObservable() {

    val name = ObservableField<String>("")
    val info = ObservableField<String>("")
    val ingredients = ObservableArrayList<AddIngredientItemViewModel>()
    val workflow = ObservableField<String>("")
    val time = ObservableField<String>("")
    val loading = ObservableBoolean(false)

    val nameError = ObservableField<String?>(null)
    val infoError = ObservableField<String?>(null)
    val workflowError = ObservableField<String?>(null)
    val timeError = ObservableField<String?>(null)

    val itemView: ItemView = ItemView.of(BR.ingredient, R.layout.item_add_ingredient)

    companion object {
        val TAG = "AddRecipeActViewModel"

        val FIELD_NAME = "cz.ackee.cookbook.addViewModel.name"
        val FIELD_INFO = "cz.ackee.cookbook.addViewModel.info"
        val FIELD_INGREDIENTS = "cz.ackee.cookbook.addViewModel.ingredients"
        val FIELD_WORKFLOW = "cz.ackee.cookbook.addViewModel.workflow"
        val FIELD_TIME = "cz.ackee.cookbook.addViewModel.time"
        val FIELD_LOADING = "cz.ackee.cookbook.addViewModel.loading"
    }

    init {
        addIngredient()
    }

    /**
     * Validates all the data of the viewmodel and sets/usets appropriate error messages
     */
    fun validate(): Boolean {
        var valid = true
        if (name.get().trim().isEmpty()) {
            nameError.set(context.getString(R.string.error_validation_name_empty))
            valid = false
        } else if (!name.get().contains("ackee", true)) {
            nameError.set(context.getString(R.string.error_validation_name_ackee))
            valid = false
        } else {
            nameError.set(null)
        }
        if (info.get().trim().isEmpty()) {
            infoError.set(context.getString(R.string.error_validation_info_empty))
            valid = false
        } else {
            infoError.set(null)
        }
        if (workflow.get().trim().isEmpty()) {
            workflowError.set(context.getString(R.string.error_validation_workflow_empty))
            valid = false
        } else {
            workflowError.set(null)
        }
        if (time.get().trim().isEmpty()) {
            timeError.set(context.getString(R.string.error_validation_time_empty))
            valid = false
        } else if (!(time.get().isNumericPositive())) {
            timeError.set(context.getString(R.string.error_validation_time_number))
            valid = false
        } else {
            timeError.set(null)
        }
        return valid
    }

    /**
     * Saves the current state of the view model as new recipe on the server
     */
    fun saveRecipe() {
        val newRecipe = Recipe()
        newRecipe.name = name.get()
        newRecipe.info = info.get()
        newRecipe.ingredients = ingredients
            .map { it.ingredientName.get() }
            .filter { !it.isEmpty() }
        newRecipe.description = workflow.get()
        try {
            newRecipe.duration = time.get().toInt()
        } catch (e: NumberFormatException) {
            newRecipe.duration = 0
        }
        loading.set(true)
        client.addRecipe(newRecipe)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ recipe ->
                    loading.set(false)
                    dataListener.saveSuccessful()
                },
                { e ->
                    Log.e(TAG, "Error when adding new recipe", e)
                    loading.set(false)
                    dataListener.showAddError(e)
                }
            )
    }

    /**
     * Adds new EditText field to fill the new ingredient to
     */
    fun addIngredient() {
        ingredients.add(AddIngredientItemViewModel())
    }

    /**
     * Retrieves LinearLayoutManager for the auto-sizing RecyclerView
     */
    fun linearLayout() : LayoutManagers.LayoutManagerFactory {
        return LayoutManagers.LayoutManagerFactory { recyclerView ->
            val linearLayoutManager = LinearLayoutManager(recyclerView.context,
                    LinearLayoutManager.VERTICAL, false)
            linearLayoutManager.isAutoMeasureEnabled = true
            linearLayoutManager
        }
    }

    /**
     * Saves the current state of the viewmodel to the given Bundle
     * (i.e. when changing configuration of the device)
     */
    fun saveState(bundle: Bundle) {
        bundle.putString(FIELD_NAME, name.get())
        bundle.putString(FIELD_INFO, info.get())
        bundle.putStringArrayList(FIELD_INGREDIENTS,
                ArrayList(ingredients.map { it.ingredientName.get() }))
        bundle.putString(FIELD_WORKFLOW, workflow.get())
        bundle.putString(FIELD_TIME, time.get())
        bundle.putBoolean(FIELD_LOADING, loading.get())
    }

    /**
     * Restores state of the viewmodel from the Bundle when applicable
     */
    fun restoreState(bundle: Bundle) {
        name.set(bundle.getString(FIELD_NAME, ""))
        info.set(bundle.getString(FIELD_INFO, ""))
        ingredients.clear()
        ingredients.addAll(bundle.getStringArrayList(FIELD_INGREDIENTS).map {
            val viewModel = AddIngredientItemViewModel()
            viewModel.ingredientName.set(it)
            viewModel
        })
        workflow.set(bundle.getString(FIELD_WORKFLOW, ""))
        time.set(bundle.getString(FIELD_TIME, ""))
        loading.set(bundle.getBoolean(FIELD_TIME, false))
    }

    /**
     * Viewmodel for the item of RecyclerView with fields for adding
     * a new ingredient
     */
    class AddIngredientItemViewModel {

        val ingredientName = ObservableField<String>("")

    }

    interface DataListener {
        /**
         * Called when save of the recipe on the server was successful
         */
        fun saveSuccessful()

        /**
         * Called when save of the recipe on the server was not successful
         */
        fun showAddError(e: Throwable)
    }

}