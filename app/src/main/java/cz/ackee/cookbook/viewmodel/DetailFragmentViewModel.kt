package cz.ackee.cookbook.viewmodel

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.databinding.ObservableFloat
import android.databinding.ObservableInt
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import cz.ackee.cookbook.BR
import cz.ackee.cookbook.R
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.model.RatingRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import me.tatarka.bindingcollectionadapter.ItemView
import me.tatarka.bindingcollectionadapter.LayoutManagers

/**
 * Created by Pavel on 25.1.2017.
 */
class DetailFragmentViewModel(private val client: CookbookClient) {

    companion object {
        val TAG = "DetailFragmentViewModel"
    }

    val name = ObservableField<String>("")
    val description = ObservableField<String>("")
    val duration = ObservableInt(0)
    val ingredients = ObservableArrayList<String>()
    val info = ObservableField<String>("")
    val score = ObservableFloat(0f)
    val myScore = ObservableInt(0)
    var itemId: String? = null

    val itemView: ItemView = ItemView.of(BR.item, R.layout.item_detail_ingredient)
    fun linearLayout() : LayoutManagers.LayoutManagerFactory {
        return LayoutManagers.LayoutManagerFactory { recyclerView ->
            val linearLayoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
            linearLayoutManager.isAutoMeasureEnabled = true
            linearLayoutManager
        }
    }

    private val disposables = CompositeDisposable()
    private var dataListener: DataListener? = null
    private var dataLoaded = false

    /**
     * Loads details of the given recipe from the server
     */
    fun loadData() {
        if (!dataLoaded) {
            val itemIdImmutable = itemId
            if (itemIdImmutable != null) {
                disposables.add(client.getRecipe(itemIdImmutable)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({recipe ->
                            name.set(recipe.name ?: "")
                            description.set(recipe.description ?: "")
                            duration.set(recipe.duration ?: 0)
                            ingredients.clear()
                            ingredients.addAll(recipe.ingredients ?: listOf())
                            info.set(recipe.info ?: "")
                            score.set(recipe.score ?: 0f)
                            myScore.set(Math.ceil(recipe.score?.toDouble() ?: 0.0).toInt())
                            dataLoaded = true
                        },
                        {e ->
                            Log.e(TAG, "Error when loading recipe", e)
                            dataListener?.showDetailError(e)
                        }
                    )
                )
            }
        }
    }

    /**
     * Adds new rating for the current recipe
     */
    fun rate(rating: Int) {
        this.myScore.set(rating)
        val itemIdImmutable = itemId
        if (itemIdImmutable != null) {
            val request = RatingRequest()
            request.score = rating
            disposables.add(client.addRating(itemIdImmutable, request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ratingResponse ->
                        score.set(ratingResponse.score ?: 0f)
                    },
                    {e ->
                        Log.e(TAG, "Error when rating recipe", e)
                        dataListener?.showRatingError(e)
                    }
                )
            )
        }
    }

    /**
     * Initializes the data of the viewModel (to have something shown before the details are loaded)
     */
    fun setDefaults(itemId: String?, defaultName: String?, defaultScore: Float?, defaultTime: Int?) {
        this.itemId = itemId
        if (defaultName != null) {
            name.set(defaultName)
        }
        if (defaultScore != null) {
            score.set(defaultScore)
            myScore.set(Math.ceil(defaultScore.toDouble()).toInt())
        }
        if (defaultTime != null) {
            duration.set(defaultTime)
        }

    }

    fun attach(dataListener: DataListener) {
        this.dataListener = dataListener
    }

    fun detach() {
        this.dataListener = null
    }

    fun destroy() {
        disposables.clear()
    }

    interface DataListener {
        /**
         * Called when loading of the details was not successful
         */
        fun showDetailError(e: Throwable)

        /**
         * Called when adding a new rating was not successful
         */
        fun showRatingError(e: Throwable)
    }

}