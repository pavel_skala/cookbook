package cz.ackee.cookbook.viewmodel

import android.content.Context
import android.content.Intent
import cz.ackee.cookbook.view.AddRecipeActivity

/**
 * Created by Pavel on 25.1.2017.
 */
class MainActivityViewModel(private val context: Context) {

    /**
     * Opens the UI for adding a new recipe
     */
    fun addRecipe() {
        context.startActivity(Intent(context, AddRecipeActivity::class.java))
    }

}