package cz.ackee.cookbook.viewmodel

import android.content.Context
import android.content.Intent
import android.databinding.ObservableField
import android.databinding.ObservableFloat
import android.databinding.ObservableInt
import cz.ackee.cookbook.model.RecipeSimple
import cz.ackee.cookbook.view.DetailActivity

/**
 * Viewmodel for item of the list of all recipes
 *
 * Created by Pavel on 25.1.2017.
 */
class RecipeItemViewModel(private val context: Context) {

    val name = ObservableField<String>("")
    val score = ObservableFloat(0f)
    val time = ObservableInt(0)
    var itemId: String? = null

    /**
     * Opens detail of the current recipe
     */
    fun onItemClicked() {
        val itemIdImmutable = itemId
        if (itemIdImmutable != null) {
            val detailIntent = Intent(context, DetailActivity::class.java)
            detailIntent.putExtra(DetailActivity.EXTRA_RECIPE_ID, itemIdImmutable)
            detailIntent.putExtra(DetailActivity.EXTRA_RECIPE_NAME, name.get())
            detailIntent.putExtra(DetailActivity.EXTRA_RECIPE_SCORE, score.get())
            detailIntent.putExtra(DetailActivity.EXTRA_RECIPE_TIME, time.get())
            context.startActivity(detailIntent)
        }
    }

    /**
     * Initializes the viewmodel with given data
     */
    fun setData(recipe: RecipeSimple) {
        name.set(recipe.name ?: "")
        score.set(recipe.score ?: 0f)
        time.set(recipe.duration ?: 0)
        itemId = recipe.id
    }

}