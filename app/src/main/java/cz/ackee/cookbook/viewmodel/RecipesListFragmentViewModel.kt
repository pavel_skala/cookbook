package cz.ackee.cookbook.viewmodel

import android.util.Log
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.model.RecipeSimple
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Pavel on 25.1.2017.
 */
class RecipesListFragmentViewModel(private val client: CookbookClient) {

    companion object {
        val TAG = "RecipesListFrViewModel"
    }

    private val disposables = CompositeDisposable()
    private var dataListener: DataListener? = null
    private var loadedData: MutableList<RecipeSimple>? = null

    /**
     * Loads the list of all recipes from the server
     */
    fun loadData(force: Boolean) {
        val loadedDataImmutable = loadedData
        if (loadedDataImmutable == null || force) {
            dataListener?.showLoading(true)
            disposables.add(client.getAllRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({result ->
                        loadedData = result.toMutableList()
                        dataListener?.showLoading(false)
                        dataListener?.dataLoaded(result)
                    },
                    {e ->
                        Log.e(TAG, "Error when loading recipes", e)
                        dataListener?.showLoading(false)
                        dataListener?.showLoadingError(e)
                    }
                )
            )
        } else {
            dataListener?.dataLoaded(loadedDataImmutable)
            dataListener?.showLoading(false)
        }
    }

    fun deleteRecipe(item: RecipeSimple, originalPosition: Int) {
        val immutableId = item.id
        if (immutableId != null) {
            disposables.add(client.deleteRecipe(immutableId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({val loadedDataImmutable = loadedData
                        if (loadedDataImmutable != null) {
                            val iterator = loadedDataImmutable.iterator()
                            while(iterator.hasNext()) {
                                val i = iterator.next()
                                if (i.id == item.id) {
                                    iterator.remove()
                                }
                            }
                        }
                    },
                    {e ->
                        Log.e(TAG, "Error when deleting recipe", e)
                        dataListener?.showDeleteError(e, item, originalPosition)
                    }
                )
            )
        }
    }

    fun attach(dataListener: DataListener) {
        this.dataListener = dataListener
    }

    fun detach() {
        this.dataListener = null
    }

    /**
     * For cleanup when view is being destroyed
     */
    fun destroy() {
        disposables.clear()
    }

    interface DataListener {

        /**
         * Called when data has been downloaded from the server
         */
        fun dataLoaded(data: List<RecipeSimple>)

        /**
         * To show or hide the loading progressbar
         * (cannot be bound using Data Binding when using SwipeRefreshLayout)
         */
        fun showLoading(loading: Boolean)

        /**
         * Called when the download of the data was not successful
         */
        fun showLoadingError(e: Throwable)

        /**
         * Called when the deletion of the item was not successful
         */
        fun showDeleteError(e: Throwable, item: RecipeSimple, originalPosition: Int)
    }

}