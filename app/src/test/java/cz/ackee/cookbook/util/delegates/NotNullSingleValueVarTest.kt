package cz.ackee.cookbook.util.delegates

import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by Pavel on 27.1.2017.
 */
class NotNullSingleValueVarTest {

    var emptyProperty: String by DelegatesExt.notNullSingleValue()
    var twoTimesProperty: String by DelegatesExt.notNullSingleValue()
    var correctProperty: String by DelegatesExt.notNullSingleValue()

    @Test(expected = IllegalStateException::class)
    fun throwWhenNotInitialized() {
        val value = emptyProperty
    }

    @Test(expected = IllegalStateException::class)
    fun throwWhenAssignedTwoTimes() {
        twoTimesProperty = "1"
        twoTimesProperty = "2"
    }

    @Test
    fun correctAssignAndRead() {
        correctProperty = "1"
        assertEquals("1", correctProperty)
    }

}