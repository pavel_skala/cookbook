package cz.ackee.cookbook.util.delegates.viewmodel

import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import cz.ackee.cookbook.R
import cz.ackee.cookbook.model.CookbookClient
import cz.ackee.cookbook.viewmodel.AddRecipeActivityViewModel
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

/**
 * Created by Pavel on 27.1.2017.
 */
class AddRecipeActivityViewModelTest {

    var client: CookbookClient = mock()
    var dataListener: AddRecipeActivityViewModel.DataListener = mock()
    var context: Context = mock()

    @Before
    fun setup() {
        client = mock()
        dataListener = mock()
        context = mock()
    }

    @Test
    fun validateAllEmpty() {
        val emptyName = "Empty name"
        val emptyInfo = "Empty info"
        val emptyWorkflow = "Empty workflow"
        val emptyTime = "Empty time"

        whenever(context.getString(R.string.error_validation_name_empty)).thenReturn(emptyName)
        whenever(context.getString(R.string.error_validation_info_empty)).thenReturn(emptyInfo)
        whenever(context.getString(R.string.error_validation_workflow_empty)).thenReturn(emptyWorkflow)
        whenever(context.getString(R.string.error_validation_time_empty)).thenReturn(emptyTime)

        val viewModel = AddRecipeActivityViewModel(client, dataListener, context)
        assertFalse(viewModel.validate(), "Validation of new viewmodel must fail")
        assertEquals(emptyName, viewModel.nameError.get())
        assertEquals(emptyInfo, viewModel.infoError.get())
        assertEquals(emptyWorkflow, viewModel.workflowError.get())
        assertEquals(emptyTime, viewModel.timeError.get())

    }

    @Test
    fun validateMissingAckee() {
        val missingName = "Missing ackee"

        whenever(context.getString(R.string.error_validation_name_ackee)).thenReturn(missingName)

        val viewModel = AddRecipeActivityViewModel(client, dataListener, context)
        viewModel.name.set("Simple name")
        assertFalse(viewModel.validate(), "Validation of new viewmodel must fail")
        assertEquals(missingName, viewModel.nameError.get())
    }

    @Test
    fun validateTimeNotNumber() {
        val wrongTime = "Wrong time"

        whenever(context.getString(R.string.error_validation_time_number)).thenReturn(wrongTime)

        val viewModel = AddRecipeActivityViewModel(client, dataListener, context)
        viewModel.time.set("NaN")
        assertFalse(viewModel.validate(), "Validation of new viewmodel must fail")
        assertEquals(wrongTime, viewModel.timeError.get())
    }

    @Test
    fun validateTimeNegativeNumber() {
        val wrongTime = "Wrong time"

        whenever(context.getString(R.string.error_validation_time_number)).thenReturn(wrongTime)

        val viewModel = AddRecipeActivityViewModel(client, dataListener, context)
        viewModel.time.set("-20")
        assertFalse(viewModel.validate(), "Validation of new viewmodel must fail")
        assertEquals(wrongTime, viewModel.timeError.get())
    }

    @Test
    fun validateOk() {
        val viewModel = AddRecipeActivityViewModel(client, dataListener, context)
        viewModel.name.set("Ackee meal")
        viewModel.info.set("Info")
        viewModel.workflow.set("Workflow")
        viewModel.time.set("20")
        assertTrue(viewModel.validate(), "Validation of new viewmodel must succeed")
        assertNull(viewModel.nameError.get())
        assertNull(viewModel.infoError.get())
        assertNull(viewModel.workflowError.get())
        assertNull(viewModel.timeError.get())
    }


// this test must be run in instrumented environment (i.e. using Robolectric
//    @Test
//    fun saveSubscribed() {
//        val recipe = Recipe()
//        whenever(client.addRecipe(any())).thenReturn(Single.just(recipe))
//        RxJavaPlugins.reset()
//        RxJavaPlugins.initIoScheduler { Schedulers.trampoline() }
//        RxAndroidPlugins.reset()
//        RxAndroidPlugins.initMainThreadScheduler { Schedulers.trampoline() }
//
//        val viewModel = AddRecipeActivityViewModel(client, dataListener, context)
//        viewModel.saveRecipe()
//
//        verify(client.addRecipe(argThat { ingredients?.isEmpty() ?: true })) // the empty igredient removed
//        assertFalse(viewModel.loading.get(), "Loading must be finished")
//        verify(dataListener.saveSuccessful())
//    }


}