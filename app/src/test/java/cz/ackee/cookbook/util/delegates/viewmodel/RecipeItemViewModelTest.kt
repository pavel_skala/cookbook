package cz.ackee.cookbook.util.delegates.viewmodel

import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import cz.ackee.cookbook.model.RecipeSimple
import cz.ackee.cookbook.viewmodel.RecipeItemViewModel
import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by Pavel on 27.1.2017.
 */
class RecipeItemViewModelTest {

    companion object {
        val RECIPE_NAME = "name"
        val RECIPE_SCORE = 3f
        val RECIPE_TIME = 20
    }

    @Test
    fun dataSet() {
        val context: Context = mock()
        val viewModel = RecipeItemViewModel(context)
        val recipe = RecipeSimple()
        recipe.name = RECIPE_NAME
        recipe.score = RECIPE_SCORE
        recipe.duration = RECIPE_TIME
        viewModel.setData(recipe)
        assertEquals(RECIPE_NAME, viewModel.name.get())
        assertEquals(RECIPE_SCORE, viewModel.score.get())
        assertEquals(RECIPE_TIME, viewModel.time.get())
    }

}